<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class NrController extends AbstractController
{
    /**
     * @Route("/nr", name="numar")
     */
    public function number()
    {
        session_start();
        $_SESSION['nae'] = random_int(0,100);
        return $this->render('number.html.twig', [
            'guess' => $_SESSION['nae'],
        ]);
    }
}
